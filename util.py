import pickle
import json
import numpy as np

__model = None

def get_estimated_infected(fever,bodypain,age,runnynose,diffbreath):
    pass


def load_saved_artifacts():
    print("loading saved artifacts...start")

    global __model
    if __model is None:
        with open('./artifacts/corona_detection_model.pickle', 'rb') as f:
            __model = pickle.load(f)
    print("loading saved artifacts...done")

if __name__ == "__main__":
    load_saved_artifacts()
    print(get_estimated_infected(99,1,29,1,1))
