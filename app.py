from flask import Flask, render_template, url_for, request, make_response
import pdfkit
import pickle
from datetime import date

import pandas as pd

import util

app = Flask(__name__)

# Use pickle to load in the pre-trained model.
with open('./artifacts/corona_detection_model_KNN.pickle', 'rb') as f:
    model = pickle.load(f)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        data = request.form
        print(data)

        fever = int(data['fever'])
        bodypain = int(data['pains'])
        if data['age'] == '':
            age = 0
        else:
            age = int(data['age'])
        runnynose = int(data['runny_nose'])
        diffbreath = int(data['difficulty_in_breathing'])

        # Make DataFrame for model
        input_variables = pd.DataFrame([[fever, bodypain, age, runnynose, diffbreath]],
                                       columns=['fever', 'bodypain', 'age', 'runnynose', 'diffbreath'],
                                       dtype=float)
        # Get the model's prediction
        prediction = model.predict(input_variables)[0]

        print(type(data))

        return render_template('result.html', data=data, prediction=prediction)
    else:
        return render_template('index.html')


@app.route('/report', methods=['POST'])
def generate_report():
    if request.method == 'POST':
        data = request.form
        shahin_date = date.today().strftime('%Y-%m-%d')
        rendered = render_template('report.html', data=data,shahin_date=shahin_date)
        pdf = pdfkit.from_string(rendered, False)
        response = make_response(pdf)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'inline; filename=report.pdf'
        return response


if __name__ == "__main__":
    # print("Starting Python Flask Server For Home Price Prediction...")
    # util.load_saved_artifacts()
    app.run(debug=True)
